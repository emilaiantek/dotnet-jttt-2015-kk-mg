﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace WindowsFormsApplication1
{
    public class Przetwarzanie
    {
        public string URL;
        public string fraza;
        public string email;

        public string poprawny_link = "http://";
        public string zrodlo = "Blank address.";
        

        public Przetwarzanie(string _url = "", string _fraza = "", string _email = "")
        {
            URL = _url; 
            fraza = _fraza;
            email = _email;
        }

        public string GetPageHtml()
        {
            using (WebClient wc = new WebClient())
            {
                // Pobieramy zawartość strony spod adresu url jako ciąg bajtów
                byte[] data = wc.DownloadData(URL);
                // Przekształcamy ciąg bajtów na string przy użyciu kodowania UTF-8. To oczywiście powinno zależeć od właściwego kodowania
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                // Wersja uproszczona bez uwzględniania kodowania:
                // string html = wc.DownloadString(url);

                return html;
            }
        }

        public void WritePageNodes()
        {
            int licznik = 0;
            HtmlDocument doc = new HtmlDocument();

            string pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");

            // Iterujemy po wszystkich znalezionych node'ach
            foreach (var node in nodes)
            {
                
                string podejrzany =  node.GetAttributeValue("alt", "");

                if (podejrzany.IndexOf(fraza) != -1)
                {
                    zrodlo = node.GetAttributeValue("src", "");
                    if (zrodlo.IndexOf(poprawny_link) != -1)
                    {

                        Console.WriteLine("#########");
                        Console.WriteLine(zrodlo);
                        Console.WriteLine("#########");

                        using (WebClient client = new WebClient())
                        {
                            String sciezka = @"C:\Users\Krzysztof\Downloads\tmp" + licznik + ".jpg";
                            client.DownloadFile(zrodlo, sciezka);
                            licznik++;

                            MailMessage mail = new MailMessage();
                            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                            mail.From = new MailAddress("dupek153@gmail.com");
                            mail.To.Add("dupek153@gmail.com");
                            mail.Subject = "Test Mail - " + licznik;
                            mail.Body = "mail with attachment";

                            System.Net.Mail.Attachment attachment;
                            attachment = new System.Net.Mail.Attachment(sciezka);
                            mail.Attachments.Add(attachment);

                            SmtpServer.Port = 587;
                            SmtpServer.Credentials = new System.Net.NetworkCredential("dupek153@gmail.com", "5hgwhds2");
                            SmtpServer.EnableSsl = true;

                            SmtpServer.Send(mail);
                        }
                    }

                }
                /*
               MailMessage message = new MailMessage(
               "testowepwr@wp.pl",
               "dupek153@gmail.com",
               "opraski",
               zrodlo);
                
                //Attachment data = new Attachment(zrodlo, MediaTypeNames.Application.Octet);
                //message.Attachments.Add(data);

                SmtpClient klient = new SmtpClient("smtp.gmail.com");
                klient.Port = 587;
                klient.EnableSsl = true;
                klient.Send(message);
                */

                
                
            }

        }
        
    }
}
