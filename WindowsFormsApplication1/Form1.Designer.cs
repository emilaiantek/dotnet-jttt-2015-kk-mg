﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbURL = new System.Windows.Forms.TextBox();
            this.tbFraza = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.Przycisk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbURL
            // 
            this.tbURL.Location = new System.Drawing.Point(98, 45);
            this.tbURL.Name = "tbURL";
            this.tbURL.Size = new System.Drawing.Size(191, 20);
            this.tbURL.TabIndex = 0;
            this.tbURL.Text = "https://www.demotywatory.pl";
            this.tbURL.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tbFraza
            // 
            this.tbFraza.Location = new System.Drawing.Point(98, 111);
            this.tbFraza.Name = "tbFraza";
            this.tbFraza.Size = new System.Drawing.Size(100, 20);
            this.tbFraza.TabIndex = 1;
            this.tbFraza.Text = "Grunt";
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(98, 195);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(100, 20);
            this.tbEmail.TabIndex = 2;
            this.tbEmail.Text = "dupek153@gmail.com";
            // 
            // Przycisk
            // 
            this.Przycisk.Location = new System.Drawing.Point(288, 195);
            this.Przycisk.Name = "Przycisk";
            this.Przycisk.Size = new System.Drawing.Size(75, 23);
            this.Przycisk.TabIndex = 3;
            this.Przycisk.Text = "Wykonaj";
            this.Przycisk.UseVisualStyleBackColor = true;
            this.Przycisk.Click += new System.EventHandler(this.Przycisk_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 262);
            this.Controls.Add(this.Przycisk);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbFraza);
            this.Controls.Add(this.tbURL);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbURL;
        private System.Windows.Forms.TextBox tbFraza;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Button Przycisk;
    }
}

